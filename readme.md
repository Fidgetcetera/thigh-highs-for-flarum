this has moved! find it at
[monsterware.dev](https://monsterware.dev/Fidgetcetera/thigh-highs-for-flarum)

This is Thigh Highs, an extremely trans themeing extension for Flarum.  

<img src="thigh-highs.svg" alt="pair of thigh highs with a transgender pride flag stripe pattern, the project icon" width="64px" /> 

Current status: **Technically You Can Install It But It Does Nothing**

Planned features:

- [ ] [Theme Permissions](https://github.com/Fidgetcetera/thigh-highs/issues/1)
- [ ] [Create individual themes](https://github.com/Fidgetcetera/thigh-highs/issues/2)
  - [ ] Add custom, swappable CSS in those themes
- [ ] [Add customizable color entry for fast-editable themes](https://github.com/Fidgetcetera/thigh-highs/issues/3)
  - [ ] with dark/light modes and an accessibility note about them
- [ ] [Allow anyone using it to change away from the default theme](https://github.com/Fidgetcetera/thigh-highs/issues/4)
